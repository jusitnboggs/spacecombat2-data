[Umemeru Ring]
Type=Ring
Pos=9157,9641,7966
Radius=6866
InnerRadius=4921
Inclination=-15
Azimuth=235
Thickness=100
MaxCount=100
Ignore Atmospheres=0

[Inyat Mineral Field]
Type=Crystal Field
Pos=-10300,-10662,9693
MaxCount=150
ScanHeight=500
RespawnRate=240
MinInitialSpawn=80
FieldHealth=500000
CrystalType=2
Ignore Atmospheres=0