[Mineral Field]
Type=Crystal Field
Pos=6500,4430,6152
MaxCount=70
MaxDistance=3000
ScanHeight=500
RespawnRate=240
MinInitialSpawn=50
FieldHealth=500000
CrystalMaterialColor=Vector(5, 0.4, 0)
CrystalEnvColor=Vector(0.75, 0.12, 0.12)
CrystalParticleColor=Color(255, 0, 0, 65)
Ignore Atmospheres=0