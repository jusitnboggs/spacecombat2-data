[Twinsuns Asteroid Belt Pt1]
Type=Belt
Pos1=-14800,-700,-2300
Pos2=-4550,9100,1900
MaxCount=20
Variance=3000
Ignore Atmospheres=0

[Twinsuns Asteroid Belt Pt2]
Type=Belt
Pos1=-4550,9100,1900
Pos2=5600,14500,-1400
MaxCount=15
Variance=3000
Ignore Atmospheres=0

[Necronis Mineral Cave]
Type=Crystal Field
Pos=10804,-9791,3958
Normal=0.0,-0.1,-1.0
MaxCount=60
ScanHeight=15
RespawnRate=240
MinInitialSpawn=60
FieldHealth=500000
CrystalMaterialColor=Vector(5, 0.8, 0)
CrystalEnvColor=Vector(0.6, 0.12, 0.12)
CrystalParticleColor=Color(255, 0, 0, 65)
Ignore Atmospheres=0

[Ice Mineral]
Type=Crystal Field
Pos=-7807, 10809, -8162
MaxCount=100
ScanHeight=15
RespawnRate=240
MinInitialSpawn=100
FieldHealth=500000
CrystalMaterialColor=Vector(0.6, 0.4, 2)
CrystalEnvColor=Vector(0.12, 0.12, 0.6)
CrystalParticleColor=Color(100, 0, 255, 65)
Ignore Atmospheres=0