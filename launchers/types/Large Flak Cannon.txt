[Configuration]
Name=Large Flak Cannon
Description=SCIENCE!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Large Burst Round=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=15

[LauncherConfiguration/Burst]
HasBurst=true
ShotsPerBurst=4
TimeBetweenBurstShots=0.25

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=32
ReloadTime=20

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=100000
CapacitorFillRate=1000
CapacitorPerShot=100

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=100
PG=1000
Slot=Large Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=true
MinimumSpread=0.05
MaximumSpread=0.085

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=30
TemperaturePerShot=0.5
CoolingRate=10

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(10000, 0, 0)