[Configuration]
Model=models/slyfo_2/pss_missilepod.mdl
FiringDirection=Angle(0,0,0)

[FiringPositions]
1=Vector(25,-22,1.5)
2=Vector(25,-18,1.5)
3=Vector(25,-14,1.5)
4=Vector(25,-22,-2.5)
5=Vector(25,-18,-2.5)
6=Vector(25,-14,-2.5)
7=Vector(25,22,1.5)
8=Vector(25,18,1.5)
9=Vector(25,14,1.5)
10=Vector(25,22,-2.5)
11=Vector(25,18,-2.5)
12=Vector(25,14,-2.5)