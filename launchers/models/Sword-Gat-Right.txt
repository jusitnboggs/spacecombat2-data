[Configuration]
Model=models/slyfo/swordgatright.mdl
FiringDirection=Angle(0,0,0)

[FiringPositions]
1=Vector(145,0,49)
2=Vector(145,7,46)
3=Vector(145,10,38)
4=Vector(145,7,31)
5=Vector(145,0,28)
6=Vector(145,-7,31)
7=Vector(145,-10,38)
8=Vector(145,-7,46)