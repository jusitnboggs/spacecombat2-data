[Configuration]
Name=Small Thermal Plasma Field
Description=A small attunement crystal that does primarily thermal damage.
Class=DamageComponent
Family=Small Plasma Field
Mass=1

[ClassData]
Damage.EM=200
Damage.THERM=1000