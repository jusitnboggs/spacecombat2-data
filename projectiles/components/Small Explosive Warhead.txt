[Configuration]
Name=Small Explosive Warhead
Description=A small warhead that does primarily explosive damage.
Class=ExplosiveComponent
Family=Small Warhead
Mass=1

[ClassData]
Radius=500
Velocity=15000
Falloff=1000
Damage.EM=2000
Damage.EXP=8000
Damage.THERM=2000