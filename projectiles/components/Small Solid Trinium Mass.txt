[Configuration]
Name=Small Solid Trinium Mass
Description=A small, heavy object made out of solid trinium.
Class=KineticDamageComponent
Family=Small Mass
Mass=9