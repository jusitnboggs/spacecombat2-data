[Configuration]
Name=Small Railgun Effect
Description=A railgun effect visual.
Class=RailgunEffectComponent
Family=Small Hybrid Container

[ClassData]
Scale=1.65
Color=Color(255, 255, 255)

[ToolOptions/Color]
Type=Color
Default=Color(255, 255, 255)