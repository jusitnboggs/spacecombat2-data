[Configuration]
Name=Medium Projectile Body
Description=A projectile visual.
Class=ModelComponent
Family=Medium Projectile

[ClassData]
Model=models/slyfo/how_explosive.mdl
Offset=Vector(0,0,0)
AngleOffset=Angle(0,0,0)
Scale=1.8

[ToolOptions/Offset]
Type=Vector
Mode=Slider
Min=-200
Max=200
Default=Vector(0,0,0)

[ToolOptions/AngleOffset]
Type=Angle
Mode=Slider
Min=-180
Max=180
Default=Angle(0,0,0)

[ToolOptions/Scale]
Type=Number
Mode=Slider
Min=1.6
Max=2.2
Default=1.8

[ToolOptions/Model]
Type=String
Mode=ComboBox
Options.1=models/slyfo/how_explosive.mdl
Options.2=models/cerus/weapons/projectiles/pc_proj.mdl
Options.3=models/slyfo_2/rocketpod_smallrocket.mdl
Options.4=models/slyfo_2/pss_thrustprojectileclosed.mdl
Options.5=models/slyfo_2/pss_gunnerprojectile.mdl
Options.6=models/slyfo/how_explosiveround.mdl
Options.7=models/slyfo_2/mortarsys_carpetbomb.mdl
Options.8=models/slyfo/how_apfsdsround.mdl
Options.9=models/slyfo/how_apfsds.mdl
Options.10=models/slyfo_2/rocketpod_bigrockethalf.mdl
Options.11=models/slyfo_2/pss_trackerprojectile.mdl
Options.12=models/slyfo_2/mortarsys_frag.mdl
Options.13=models/slyfo_2/pss_weightprojectile.mdl
Options.14=models/slyfo/rover_winchhookclosed.mdl
Options.15=models/slyfo_2/pss_gasserprojectile.mdl
Default=models/slyfo/how_explosive.mdl