[Configuration]
Name=Large Pulse Round
OwnerID=NULL
ProjectileType=Large Shielded Kinetic Round

[Components/Payload]
Component=Large Solid Nubium Mass

[Components/Field]
Component=Large EM Plasma Field

[Components/Body]
Component=Large Pulse Effect
Configuration.Color=Color(200,100,255)

[Components/Trail]
Component=Large Trail
Configuration.Color=Color(200,100,255)
Configuration.Length=300
Configuration.Width=100

[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_pulsecannon