[Configuration]
Name=Tiny Kinetic Cannon Round
OwnerID=NULL
ProjectileType=Tiny Cannon Round

[Components/Payload]
Component=Tiny Solid Trinium Mass

[Components/Body]
Component=Tiny Projectile Body
Configuration.Model=models/slyfo/how_apfsds.mdl

[Components/Trail]
Component=Tiny Trail
Configuration.Color=Color(255, 205, 0)
Configuration.Length=200
Configuration.Width=60

[Components/Hit Effect]
Component=Tiny Legacy Hit Effect
Configuration.HitEffect=impact_autocannon