[Configuration]
Name=Medium Solid Trinium Cannon Round
OwnerID=NULL
ProjectileType=Medium Cannon Round

[Components/Payload]
Component=Medium Solid Trinium Mass

[Components/Body]
Component=Medium Projectile Body

[Components/Hit Effect]
Component=Medium Legacy Hit Effect
Configuration.HitEffect=impact_artillery