[Configuration]
Name=Large Explosive Burst Round
OwnerID=NULL
ProjectileType=Large Burst Round

[Components/Payload]
Component=Large Explosive Warhead

[Components/Body]
Component=Large Projectile Body
Configuration.Model=models/slyfo/how_explosive.mdl
Configuration.Offset=Vector(0,0,0)
Configuration.AngleOffset=Angle(0,0,0)
Configuration.Scale=2

[Slots/Timer]
Component=Large Detonation Timer
Configuration.Delay=3

[Components/Trail]
Component=Large Trail
Configuration.Color=Color(255,200,175)
Configuration.Length=400
Configuration.Width=60
Configuration.Material=trails/smoke.vmt

[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_medmissile