[Configuration]
Name=Large Mjolnir
OwnerID=NULL
ProjectileType=Large Missile

[Components/Payload]
Component=Large EM Warhead

[Components/Body]
Component=Large Mjolnir Effect
Configuration.Color=Color(50, 100, 255)

[Components/Thrusters]
Component=Large Homing Thruster

[Components/Trail]
Component=Large Trail
Configuration.Color=Color(150, 150, 255)
Configuration.Length=500
Configuration.Width=300

[Components/Hit Effect]
Component=Large Mjolnir Hit Effect
Configuration.Color=Color(60, 160, 255)