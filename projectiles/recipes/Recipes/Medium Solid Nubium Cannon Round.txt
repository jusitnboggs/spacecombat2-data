[Configuration]
Name=Medium Solid Nubium Cannon Round
OwnerID=NULL
ProjectileType=Medium Cannon Round

[Components/Payload]
Component=Medium Solid Nubium Mass

[Components/Body]
Component=Medium Projectile Body

[Components/Hit Effect]
Component=Medium Legacy Hit Effect
Configuration.HitEffect=impact_autocannon