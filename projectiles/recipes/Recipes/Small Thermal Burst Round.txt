[Configuration]
Name=Small Thermal Burst Round
OwnerID=NULL
ProjectileType=Small Burst Round

[Components/Payload]
Component=Small Thermal Warhead

[Components/Body]
Component=Small Projectile Body
Configuration.Model=models/slyfo/how_explosive.mdl
Configuration.Offset=Vector(0,0,0)
Configuration.AngleOffset=Angle(0,0,0)
Configuration.Scale=1

[Slots/Timer]
Component=Small Detonation Timer
Configuration.Delay=3

[Components/Trail]
Component=Small Trail
Configuration.Color=Color(255,200,175)
Configuration.Length=400
Configuration.Width=60
Configuration.Material=trails/smoke.vmt

[Components/Hit Effect]
Component=Small Legacy Hit Effect
Configuration.HitEffect=impact_medmissile