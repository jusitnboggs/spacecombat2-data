[Configuration]
Name=Small EM Cannon Round
OwnerID=NULL
ProjectileType=Small Cannon Round

[Components/Payload]
Component=Small EM Warhead

[Components/Body]
Component=Small Projectile Body

[Components/Hit Effect]
Component=Small Legacy Hit Effect
Configuration.HitEffect=impact_railgun
