[Configuration]
Name=Large Homing Kinetic Missile
OwnerID=NULL
ProjectileType=Large Missile

[Components/Payload]
Component=Large Solid Nubium Mass

[Components/Body]
Component=Large Missile Body

[Components/Thrusters]
Component=Large Homing Thruster

[Components/Trail]
Component=Large Missile Trail

[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_artillery