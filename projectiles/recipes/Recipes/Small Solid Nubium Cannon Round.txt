[Configuration]
Name=Small Solid Nubium Cannon Round
OwnerID=NULL
ProjectileType=Small Cannon Round

[Components/Payload]
Component=Small Solid Nubium Mass

[Components/Body]
Component=Small Projectile Body

[Components/Hit Effect]
Component=Small Legacy Hit Effect
Configuration.HitEffect=impact_autocannon