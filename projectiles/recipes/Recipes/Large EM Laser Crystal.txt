[Configuration]
Name=Large EM Laser Crystal
OwnerID=NULL
ProjectileType=Large Laser Crystal

[Components/Attunement Crystal]
Component=Large EM Attunement Crystal

[Components/Beam Modulator]
Component=Large Beam Effect


[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_smallbeam
