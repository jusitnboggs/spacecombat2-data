[Configuration]
Name=Medium Thermal Burst Round
OwnerID=NULL
ProjectileType=Medium Burst Round

[Components/Payload]
Component=Medium Thermal Warhead

[Components/Body]
Component=Medium Projectile Body
Configuration.Model=models/slyfo/how_explosive.mdl
Configuration.Offset=Vector(0,0,0)
Configuration.AngleOffset=Angle(0,0,0)
Configuration.Scale=1.5

[Slots/Timer]
Component=Medium Detonation Timer
Configuration.Delay=3

[Components/Trail]
Component=Medium Trail
Configuration.Color=Color(255,200,175)
Configuration.Length=400
Configuration.Width=60
Configuration.Material=trails/smoke.vmt

[Components/Hit Effect]
Component=Medium Legacy Hit Effect
Configuration.HitEffect=impact_medmissile