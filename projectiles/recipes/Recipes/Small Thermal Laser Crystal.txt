[Configuration]
Name=Small Thermal Laser Crystal
OwnerID=NULL
ProjectileType=Small Laser Crystal

[Components/Attunement Crystal]
Component=Small Thermal Attunement Crystal

[Components/Beam Modulator]
Component=Small Beam Effect

[Components/Hit Effect]
Component=Small Legacy Hit Effect
Configuration.HitEffect=impact_smallbeam
