[Configuration]
Name=Large EM Cannon Round
OwnerID=NULL
ProjectileType=Large Cannon Round

[Components/Payload]
Component=Large EM Warhead

[Components/Body]
Component=Large Projectile Body

[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_railgun
