[Configuration]
Name=Medium Thermal Missile
OwnerID=NULL
ProjectileType=Medium Missile

[Components/Payload]
Component=Medium Thermal Warhead

[Components/Body]
Component=Medium Missile Body

[Components/Thrusters]
Component=Medium Homing Thruster

[Components/Trail]
Component=Medium Missile Trail

[Components/Hit Effect]
Component=Medium Legacy Hit Effect
Configuration.HitEffect=impact_medmissile