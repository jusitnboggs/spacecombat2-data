[Configuration]
Name=Medium Heavy Kinetic Torpedo
OwnerID=NULL
ProjectileType=Medium Missile

[Components/Payload]
Component=Medium Solid Trinium Mass

[Components/Body]
Component=Medium Missile Body
Configuration.String=models/slyfo/d12guidedbomb.mdl

[Components/Thrusters]
Component=Medium Thruster

[Components/Trail]
Component=Medium Missile Trail

[Components/Hit Effect]
Component=Medium Legacy Hit Effect
Configuration.HitEffect=impact_autocannon