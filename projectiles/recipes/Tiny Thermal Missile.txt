[Configuration]
Name=Tiny Thermal Missile
OwnerID=NULL
ProjectileType=Tiny Missile

[Components/Payload]
Component=Tiny Thermal Warhead

[Components/Body]
Component=Tiny Missile Body

[Components/Thrusters]
Component=Tiny Homing Thruster

[Components/Trail]
Component=Tiny Missile Trail

[Components/Hit Effect]
Component=Tiny Legacy Hit Effect
Configuration.HitEffect=impact_medmissile