[Configuration]
Name=Large EM Missile
OwnerID=NULL
ProjectileType=Large Missile

[Components/Payload]
Component=Large EM Warhead

[Components/Body]
Component=Large Missile Body

[Components/Thrusters]
Component=Large Homing Thruster

[Components/Trail]
Component=Large Missile Trail

[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_medmissile