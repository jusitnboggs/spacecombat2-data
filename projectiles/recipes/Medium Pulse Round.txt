[Configuration]
Name=Medium Pulse Round
OwnerID=NULL
ProjectileType=Medium Shielded Kinetic Round

[Components/Payload]
Component=Medium Solid Nubium Mass

[Components/Field]
Component=Medium EM Plasma Field

[Components/Body]
Component=Medium Pulse Effect
Configuration.Color=Color(200,100,255)

[Components/Trail]
Component=Medium Trail
Configuration.Color=Color(200,100,255)
Configuration.Length=300
Configuration.Width=100

[Components/Hit Effect]
Component=Medium Legacy Hit Effect
Configuration.HitEffect=impact_pulsecannon