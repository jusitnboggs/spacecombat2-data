[Configuration]
Name=Large Hybrid Charge
Description=A Large projectile that can equip homing thrusters and various warheads.

[Slots/Payload]
Large Hybrid=true

[Slots/Body]
Large Hybrid Container=true

[Slots/Trail]
Large Trail=true

[Slots/Hit Effect]
Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component