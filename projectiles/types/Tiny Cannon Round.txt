[Configuration]
Name=Tiny Cannon Round
Description=A tiny cannon round that can equip homing thrusters and either artificial mass or warheads.

[Slots/Payload]
Tiny Warhead=true
Tiny Mass=true

[Slots/Body]
Tiny Projectiles=true

[Slots/Trail]
Tiny Trail=true

[Slots/Hit Effect]
Tiny Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component