[Configuration]
Name=Small Shielded Kinetic Round
Description=A small kinetic projectile that is surrounded by a field of plasma.

[Slots/Payload]
Small Mass=true

[Slots/Field]
Small Plasma Field=true

[Slots/Body]
Small Shielded Round=true

[Slots/Trail]
Small Trail=true

[Slots/Hit Effect]
Small Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component