[Configuration]
Name=Large Plasma Container
Description=A plasma container that can be fired in many forms.

[Slots/Payload]
Large Plasma=true

[Slots/Body]
Large Plasma Body=true

[Slots/Hit Effect]
Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component